01/04/2021 :
 - modification de la requête en ne prenant que ceux qui ont un badge
 09/04/2021 
 - retour sur la modification précédente des utilisateurs qui n'ont pas de badge
 16/04/2021
 -	on vide le fichier log de prod de la veille : export.log  avant de lancer l'exécution du script
 - on vide aussi les erreurs de la veille dans le fichier refId_log.out avant de lancer le script d'envoi de mail
 - on envoie les erreurs par mail à gerarld avec le script mail.sh
 22/04/2021
 - déploiement d'un nouveau jar correction faute d'orthographe dans les logs
 20/05/2021
 - séparation entre l'export alma et l'export vers pc securité.
 Désormais ce programme n'exporte que vers alma. J'ai commenté l'export vers le pc securité. ce dernier est fait dans
 un projet java indépendant: exportpcsecurite
 