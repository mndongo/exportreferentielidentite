package com.campus.condorcet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//on prod use schema : test_sch
@Table(name = "xmluserexported")
//@Table(name = "test_sch.xmluserexported")
public class XmlDataExport {

	@Id
    @Column(name = "UID", nullable = false)
	private String uid;
	
	@Column(name="dateexport")
	private String dateexport;
	
	@Column(name="mail")
	private String mail;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getDateeexport() {
		return dateexport;
	}

	public void setDateeexport(String dateexport) {
		this.dateexport = dateexport;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	}
