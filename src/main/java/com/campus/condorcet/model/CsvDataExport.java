package com.campus.condorcet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//on prod use schema : test_sch
@Table(name = "userexported")
//@Table(name = "test_sch.userexported")
public class CsvDataExport {

	@Id
    @Column(name = "UID", nullable = false)
	private String uid;
	
	@Column(name="dateexport")
	private String dateExport;
	
	@Column(name="mail")
	private String mail;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getDateExport() {
		return dateExport;
	}

	public void setDateExport(String dateExport) {
		this.dateExport = dateExport;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
}
