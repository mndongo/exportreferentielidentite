package com.campus.condorcet.main;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.campus.condorcet.service.ILdapExportService;


public class ExportReferentiel {
	
	private static final Logger logger = Logger.getLogger(ExportReferentiel.class);
	// modificationn24-02-2019 : ajout des profils exterieurs EXT dans la liste des établissements
	// modificationn26-03-2021 : YYYYMMJJ-Z dans le ldap mais il faut la modifier et la transformer en YYYY-MM-JJ
	public static void main(String[] args) {
		String date = null;
		String operator=null;
		FileInputStream fis =null;
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:applicationContext.xml");
        ILdapExportService ldapExportService = (ILdapExportService) context.getBean("ldapService");
        ILdapExportService controleAcces = (ILdapExportService) context.getBean("controlAccesService");
       
        Properties properties = new Properties();
        logger.info("++++++ start +++++++");
        try {
        	for(int i=0;i< args.length;i++) {
            	logger.info("arg ["+i+"] = "+ args[i] );
            	operator = args[1];
            	date = args[2];
            	logger.info("arg ["+i+"] = "+args[i]) ;
            }
        	fis = new FileInputStream(new File(args[0]));
      
        	properties.load(fis);
        }catch(ArrayIndexOutOfBoundsException e) {
        	logger.error("size is : "+args.length);
        }
        catch (IOException e) {
            
            e.printStackTrace();
        }finally {
        	try {
        		fis.close();
        		
        	}catch (Exception e) {
				logger.info("erreur lors de la fermeture du fichier : "+e.getStackTrace());
			}
		}
        logger.info("before requeting  operator is : "+operator+ " date modification is : "+date);
        logger.info("------- LOADING EXPORT ALMA ------");
        ldapExportService.loadReferentielUser(properties,operator,date);
       /* try {
        	logger.info("**** LOADING CONTROL ACCESS ***** ");
        	controleAcces.loadReferentielUser(properties, operator, date);
		}catch (Exception e) {
			logger.error("+++++++ ERROR ++++++"+e);
		}*/
        
        logger.info("finish to run");
       
       
	}

}
