package com.campus.condorcet.dao;

public interface IReferentielIdentiteDao<T>{
	
	T findbyId(T t);
	  
}
