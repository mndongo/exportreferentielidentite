package com.campus.condorcet.dao.impl;

import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.campus.condorcet.dao.IUserDao;
import com.campus.condorcet.model.User;

@Component("userDao")
public class UserDaoImpl extends ReferenceIdentityDaoImpl<User> implements IUserDao{

	public UserDaoImpl(Class<User> user) {
		super(user);
	}
	public UserDaoImpl (){
		
	}
	public User loadUserByUsername(String firstName) {
		System.out.println("**** methode nouvellement cr��e");
		Query query = this.emf.createEntityManager().createQuery("select u FROM User u where u.firstName= :firstName");
        query.setParameter("firstName", firstName);
        List<User> users = query.getResultList();
        System.out.println("size = "+users.size());
        if (users != null && users.size() == 1) {
            return users.get(0);
        }
		return null;
	}
	public List<User> loadUser() {
		Query query = this.emf.createEntityManager().createQuery("FROM User ");
		List<User> users = query.getResultList();
        System.out.println("size = "+users.size());
       /* users.stream().forEach((u)->{
        	System.out.println("nom = "+u.getFirstName()+ " prenom = "+u.getLastName()+" email = "+u.getEmail());   
        });*/
		return users;
	}

	
}
