package com.campus.condorcet.dao.impl;

import java.util.HashMap;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.campus.condorcet.dao.IControleAccesServiceDao;
import com.campus.condorcet.model.CsvDataExport;
import com.campus.condorcet.model.LastExecutionCsv;
import com.campus.condorcet.model.LdapExport;

@Component("csvDao")
public class ControleAccesDaoImpl extends ReferenceIdentityDaoImpl<CsvDataExport> implements IControleAccesServiceDao{

	private static final Logger logger = Logger.getLogger(ControleAccesDaoImpl.class);

	public ControleAccesDaoImpl() {
		super();
	}
	
	public ControleAccesDaoImpl(Class<CsvDataExport> csvDataExport) {
		super(csvDataExport);
	
	}
	@Override
	@Transactional
	public LastExecutionCsv getLastExecution() {
		try {			
			logger.info("++++ start getLastUpdate +++++");
			//entityManager.getTransaction().begin();
			String qlQuery = "SELECT l FROM LastExecutionCsv l";
			Query query = this.emf.createEntityManager().createQuery("select l FROM LastExecutionCsv l");
			LastExecutionCsv lastRun = (LastExecutionCsv) query.getSingleResult();
			logger.info("result = "+ lastRun.getLastdateexecution());
			return lastRun;
		}catch(Exception e) {
			logger.info("error sql = "+e.getMessage());
			
		}
		
		return null;
	}
	/*** modification 10-02-2021 : on ne compare plus avec le sutilisateurs déjà 
	exportés seulement on n'exporte que les utilisateurs modifiés 2 jours avant
	je fais un merge plutôt qu'un persiste
*/
	@Override
	@Transactional
	public void save(CsvDataExport csvDataExport) {
		this.entityManager.merge(csvDataExport);
	}

	@Override
	public HashMap<String, String> loadCsvDataExport() {
		HashMap<String,String> hasmapUserExporded = new HashMap<String, String>();
		Query query = this.emf.createEntityManager().createQuery("FROM CsvDataExport");
		List<CsvDataExport> users = query.getResultList();
		for(CsvDataExport userCsv : users) {
			hasmapUserExporded.put(userCsv.getUid(), userCsv.getDateExport());
		}
		return hasmapUserExporded;
		
		
	}

	@Override
	public List<LdapExport> loadAll() {
		Query query = this.emf.createEntityManager().createQuery("FROM LdapExport");
		List<LdapExport> users = query.getResultList();
		return users;
	}

	@Override
	public List<CsvDataExport> getallUserExported() {
		Query query = this.emf.createEntityManager().createQuery("FROM CsvDataExport");
		List<CsvDataExport> users = query.getResultList();
		return users;
	}

	@Override
	public List<LdapExport> loadAllByDateModification(String paramModification, String operator) {
		StringBuffer sb = new StringBuffer();
		sb.append(" FROM LdapExport ");
		sb.append(" where modifytimestamp "+operator+ "'"+paramModification+ "'" );
		//sb.append(" AND fdbadge is not null");
		Query query = this.emf.createEntityManager().createQuery(sb.toString());
		List<LdapExport> users = query.getResultList();
		return users;
	}
	

	
}
