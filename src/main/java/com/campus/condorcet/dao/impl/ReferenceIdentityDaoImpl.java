package com.campus.condorcet.dao.impl;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.campus.condorcet.dao.IReferentielIdentiteDao;
@Repository
public abstract class ReferenceIdentityDaoImpl<T> implements IReferentielIdentiteDao<T>{
	
	@Autowired
	protected EntityManagerFactory emf;
	
	private Class<T> type;
	
	@PersistenceUnit
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }
   
	@PersistenceContext
	protected EntityManager entityManager;
	
	public ReferenceIdentityDaoImpl() {
		
	}
	
	public ReferenceIdentityDaoImpl(Class<T> clazz) {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
 		this.type = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	public T findbyId(T id) {
		
		EntityManager  entityManager = emf.createEntityManager();
		return (T) entityManager.find(type, id);
		
	}
	
}
