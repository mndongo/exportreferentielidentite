package com.campus.condorcet.dao.impl;

import java.util.HashMap;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.campus.condorcet.dao.ILdapExportInterfaceDao;
import com.campus.condorcet.model.LdapExport;
import com.campus.condorcet.model.XmlDataExport;

@Component("ldapDao")
public class LdapExportDaoImpl extends ReferenceIdentityDaoImpl<LdapExport> implements ILdapExportInterfaceDao {

	private static final Logger logger = Logger.getLogger(LdapExportDaoImpl.class);
	
	public LdapExportDaoImpl(Class<LdapExport> ldapExport) {
		super(ldapExport);
	
	}
	
	public LdapExportDaoImpl() {
		
	}

	@Override
	public List<LdapExport> loadAll() {
		
		Query query = this.emf.createEntityManager().createQuery("FROM LdapExport");
		List<LdapExport> users = query.getResultList();
		return users;
	}

	@Override
	public List<LdapExport> loadAllBy(String param,int size) {
		StringBuffer sb = new StringBuffer();
		sb.append(" FROM LdapExport order by "+ param +" desc");
		Query query = this.emf.createEntityManager().createQuery(sb.toString());
		//query.setParameter(1,param);
		query.setMaxResults(size);
		List<LdapExport> users = query.getResultList();
		return users;
	}

	@Override
	public List<LdapExport> loadAllByDateModification(String paramModification,String op,int size) {
		logger.info("loadAllByDateModification : load user by date modification");
		StringBuffer sb = new StringBuffer();
		sb.append(" FROM LdapExport ");
		sb.append(" WHERE modifytimestamp "+op+ "'"+paramModification+ "'" );
		//sb.append(" AND fdbadge is not null");
		Query query = this.entityManager.createQuery(sb.toString());
		List<LdapExport> users = query.getResultList();
		return users;
	}

	public List<LdapExport> loadAllByDateCreation(String paramModification,String op,int size) {
		StringBuffer sb = new StringBuffer();
		sb.append(" FROM LdapExport ");
		sb.append(" where createtimestamp "+op+ "'"+paramModification+ "'" );
		Query query = this.emf.createEntityManager().createQuery(sb.toString());
		//query.setParameter(1,param);
		query.setMaxResults(size);
		List<LdapExport> users = query.getResultList();
		return users;
	}

	@Override
	@Transactional
	public void save(XmlDataExport xmlDataExport) {
		this.entityManager.merge(xmlDataExport);
	}

	@Override
	public HashMap<String, String> loadXmlDataExport() {
		HashMap<String,String> hasmapUserExporded = new HashMap<String, String>();
		Query query = this.emf.createEntityManager().createQuery("FROM XmlDataExport");
		List<XmlDataExport> users = query.getResultList();
		for(XmlDataExport userCsv : users) {
			hasmapUserExporded.put(userCsv.getUid(), userCsv.getDateeexport());
		}
		return hasmapUserExporded;
		
		
	}

	@Override
	public LdapExport findbyId(LdapExport t) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
