package com.campus.condorcet.dao;

import java.util.HashMap;
import java.util.List;

import com.campus.condorcet.model.CsvDataExport;
import com.campus.condorcet.model.LdapExport;
import com.campus.condorcet.model.XmlDataExport;

public interface ILdapExportInterfaceDao extends IReferentielIdentiteDao<LdapExport>{

	public List<LdapExport> loadAll();
	public List<LdapExport> loadAllBy(String param,int size);
	public List<LdapExport> loadAllByDateModification(String paramModificationn,String operator, int size);
	public List<LdapExport> loadAllByDateCreation(String dateCreation,String operator, int size);
	public HashMap<String,String> loadXmlDataExport();
	public void save(XmlDataExport csvDataExport) ;
}
