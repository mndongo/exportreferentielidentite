package com.campus.condorcet.exception;

public class SuppanAffectationException extends Exception{

	public SuppanAffectationException(String s) {
		super(s);
	}
	
	public SuppanAffectationException(String s,Throwable t) {
		super(s,t);
	}
	
}
