package com.campus.condorcet.utils;

public enum CategorieUsers {

	CNRS,
	
	EHESS,
	
	ENC,
	
	EPCC,
	
	EPHE,
	
	FMSH,
	
	HAP,
	
	INED,
	
	PARIS1,
	
	PARIS3,
	
	PARIS8,
	
	PARIS10,
	
	PARIS13
}
