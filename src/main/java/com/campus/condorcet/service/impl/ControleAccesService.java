package com.campus.condorcet.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.FileAlreadyExistsException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.persistence.PersistenceException;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.hibernate.TransactionException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.campus.condorcet.dao.IControleAccesServiceDao;
import com.campus.condorcet.exception.SuppanAffectationException;
import com.campus.condorcet.model.CsvDataExport;
import com.campus.condorcet.model.LastExecutionCsv;
import com.campus.condorcet.model.LdapExport;
import com.campus.condorcet.utils.RefIdConstants;

import sun.net.ftp.FtpLoginException;
@Service("controlAccesService")
public class ControleAccesService extends LdapExportService{
	
	@Autowired
	IControleAccesServiceDao csvdao;
	
	public void setCsvdao(IControleAccesServiceDao csvdao) {
		this.csvdao = csvdao;
	}

	private static final Logger logger = Logger.getLogger(ControleAccesService.class);
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	private static final DateTimeFormatter dateFormat8 = DateTimeFormatter.ofPattern(DATE_FORMAT);
	
	public List<LdapExport> loadReferentielUser(Properties properties,String op,String date) {
		
		List<LdapExport> lUtilisateurCsv= null;
		HashMap<String, String> hUserDejaExporte = new HashMap<String, String>();
		
		logger.info("get service contrôle access");
		
		try {
				StringBuilder dateDuJour = TraitementUtil.getdateDuJourFormatAlma();
				//lUtilisateurCsv= csvdao.loadAll();
				lUtilisateurCsv= csvdao.loadAllByDateModification(date, op);
				logger.info("nbre total utilisateur = "+lUtilisateurCsv.size());
				if(lUtilisateurCsv.size()==0) {
					logger.info("Il n'y pas d'utilisateur a exporté durant la période : ");
					
				}else {
					Set<LdapExport> sUserdoNotExport = new HashSet<LdapExport>();
					Set<LdapExport> sUserNotHavingEduPeronnAfiliation = new HashSet<LdapExport>();
					
					sUserdoNotExport= getUserNotExported(lUtilisateurCsv, properties);
					
					for (LdapExport ldapExport : sUserdoNotExport) {
						if(lUtilisateurCsv.contains(ldapExport)) {
							logger.error("Export PC Securite : utilisateur a supprimer de la liste initiale car pas suppanentiteaffectationn uid : "+ldapExport.getUid()+ " mail ="+ldapExport.getMail());
							//logger.info("utilisateur à supprimer de la liste initiale : "+ldapExport.getCn()+ " "+ldapExport.getMail());
							lUtilisateurCsv.remove(ldapExport);
						}
				}
				logger.error("*** Export PC Sécurite :  Nombre d'utilistateur total n'ayant pas de suppanentiteaffectationn et ne sera pas exporte : "+sUserdoNotExport.size());
				/*** pour le moment on applique pas la qui a été faite pour l'export alma
				 * modification 10-02-2021 : on ne compare plus avec le sutilisateurs déjà
				 * exportés seulement on n'exporte que les utilisateurs modifiés 2 jours avant
				 * 
				 */
				
				// récupération des utilisateurs déjà exportés
				/*hUserDejaExporte = csvdao.loadCsvDataExport();
				logger.info("§§§§§§ ControleAccesService : Le nombre d'utilisateur déjà exporté est : "+hUserDejaExporte.size());
				
				Iterator<LdapExport> it = lUtilisateurCsv.iterator();
				while(it.hasNext()) {
				
					if(hUserDejaExporte.containsKey(it.next().getUid())){
						it.remove();
					}
				}*/
				logger.info("---- ControleAccesService : restant à exporter en csv aprés vérification sur ceux déjà exportés est : "+lUtilisateurCsv.size());
				
				
				
				initUser(properties, lUtilisateurCsv, dateDuJour, sUserNotHavingEduPeronnAfiliation);
				
				// suppression des user qui n'ont pas d'affiliation			
				for (LdapExport ldapExportNotHavingEduPersonAffiliation : sUserNotHavingEduPeronnAfiliation) {
					if(lUtilisateurCsv.contains(ldapExportNotHavingEduPersonAffiliation)) {
						lUtilisateurCsv.remove(ldapExportNotHavingEduPersonAffiliation);
					}
				}
				
				// group GED-LECTEURS
				List<LdapExport> groupGedLecteur = new ArrayList<LdapExport>();
				for(LdapExport auser : lUtilisateurCsv) {
					if(auser.getOu() != null && auser.getOu().equals("GED-LECTEURS")) {
						groupGedLecteur.add(auser);
					}
				}
				logger.info("+++ nbre d'utilisateur dont le Ou est GED-LECTEURS = "+groupGedLecteur.size());
				
				// suppression des utilisateurs du group ged lecteurs		
				for (LdapExport gedLecteur : groupGedLecteur) {
					if(lUtilisateurCsv.contains(gedLecteur)) {
						lUtilisateurCsv.remove(gedLecteur);
					}
				}
				if(lUtilisateurCsv.size()==0) {
					logger.debug("ControleAccesService : Après traitement, il n'y a pas d'export csv ");
				}else {
					// sauvegade des utilisateur exportés
					logger.info("nombre d'utilisateur a sauvegarder = "+lUtilisateurCsv.size());
					saveUserExported(lUtilisateurCsv);
					createTemplateType(lUtilisateurCsv);
					
					logger.info("nbre d'utilisateur crée pour l'export csv = "+lUtilisateurCsv.size());
					
				}
				
			}
			
				/**
				 * construction de  la template pour tous les utilisateurs exportés
				 */
				List<CsvDataExport> allUtilisateurCsv = csvdao.getallUserExported();
				logger.info("size for all user csv exported = "+allUtilisateurCsv.size());
				
				createTemplateTypeforAllUser(allUtilisateurCsv);
		 
	}catch (DateTimeException e) {
		logger.error("erreur lors de la creation de la date");
	}catch(TransactionException e) {
		logger.error("problème de connexion à la base");
	}
	catch (Exception e) {
		e.printStackTrace();
		logger.info(" error = " +e);
	}
	return null;
	}


	/**
	 * crée la template pour tous les utilisateurs déjà exportés
	 * @param allUtilisateurCsv
	 */
	private void createTemplateTypeforAllUser(List<CsvDataExport> allUtilisateurCsv) {
		logger.info("ControleAccesService : Start createTemplateCsv");
		/**
		 * Initialize engine and get template
		 */
		
		try{
			Properties p = new Properties();	
			p.setProperty("resource.loader", "class");
			p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			Velocity.init( p );
			VelocityContext context = new VelocityContext();
						
			Template templatecsv = Velocity.getTemplate("templates/allUserCsvExported.vm");
			
			if(templatecsv != null) {
				context.put("userList", allUtilisateurCsv);	
				// chargement de la template
				loadTemplateForAll(templatecsv, context);
			}
		}catch( ResourceNotFoundException rnfe ){
			logger.error("la ressource demandée n'existe pas " +rnfe);
		}catch( ParseErrorException pee ) {
			logger.error("une erreur est survenue lors du parsing de la template " +pee);
		}catch( MethodInvocationException mie ){
			logger.info("erreur est apparue lors d'un appel à une directive "+mie);

		}
		
	}


	private void saveUserExported(List<LdapExport> lUtilisateur) throws ConstraintViolationException, PersistenceException, Exception {
		String dateDuJour = TraitementUtil.getdateDuJourFormat();
			HashSet<LdapExport> setUtilisateur = new HashSet<LdapExport>();
			Iterator<LdapExport> it = lUtilisateur.iterator();
			while(it.hasNext()) {
				setUtilisateur.add(it.next());
			}
			for(LdapExport exp : setUtilisateur) {
				CsvDataExport csvDataExport = new CsvDataExport();
				csvDataExport.setUid(exp.getUid());
				csvDataExport.setDateExport(dateDuJour);
				csvDataExport.setMail(exp.getMail());
				csvdao.save(csvDataExport);
				
			}
		
	}

	@Override
	public void createTemplateType(List<LdapExport> listUser) {
		logger.info("ControleAccesService : Start createTemplateXm ***l");
		/**
		 * Initialize engine and get template
		 */
		
		try{
			Properties p = new Properties();	
			p.setProperty("resource.loader", "class");
			p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			Velocity.init( p );
			VelocityContext context = new VelocityContext();
						
			Template templatecsv = Velocity.getTemplate("templates/user.vm");
			//Template templatecsv = Velocity.getTemplate("templates/export.vm");
			
			
			if(templatecsv != null) {
				context.put("userList", listUser);	
				// chargement de la template
				loadTemplateT(templatecsv, context);
			}
		}catch( ResourceNotFoundException rnfe ){
			logger.error("la ressource demandée n'existe pas " +rnfe);
		}catch( ParseErrorException pee ) {
			logger.error("une erreur est survenue lors du parsing de la template " +pee);
		}catch( MethodInvocationException mie ){
			logger.info("erreur est apparue lors d'un appel à une directive "+mie);

		}
	     
		
	}

	/**
	 * 
	 */
	@Override
	public void loadTemplateT(Template template, VelocityContext context) {
		
		try {
				Calendar cal = Calendar.getInstance(); cal.setTime(Date.from(Instant.now()));
				
				String format = String.format( RefIdConstants.FORMAT, cal);
				StringBuilder builder = new StringBuilder();
				builder.append(folderLocation);
				
				builder.append(format);
				builder.append(RefIdConstants.REF_ID_EXPORT_CSV);
				
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(builder.toString()),"ISO-8859-1"));
				
				template.merge(context, writer);
	
				writer.flush();
				writer.close();
				//TODO à voir si REF_ID_EXPORT_CSV REF_ID_EXPORT_ALL_CSV dans le cadre de la creation de tpus les utilisateurs 
				File fileCreated = new File(folderLocation+format+RefIdConstants.REF_ID_EXPORT_CSV);
				if(fileCreated.exists()) {
					logger.info("file is created");
				}else {
					logger.error("could not created");
			}
			//pas besoin de zipper le fichier ni de faire l'export pour le csv
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("le fichier n'est pas créé "+e.getMessage());
		}catch (FileAlreadyExistsException  e) {
			logger.error("file already exist");
		}catch (FtpLoginException e) {
			logger.error("connexion serveur erreur");
		}catch (Exception e) {
			logger.info(e);
		}
	}
	
	
	public void getAffiliation(LdapExport user,Properties properties) {
		
		TraitementUtil.deleteCrochetForUser(user);
		StringBuilder sbAffiliation = new StringBuilder(user.getEduPersonAffiliation().trim());
		StringBuilder sbEtablissement = new StringBuilder(user.getSupannentiteaffectation());
		PropertiesConfiguration p = null;
		List<String> lEdupersonAffiliation = TraitementUtil.getStringToList(sbAffiliation);
		List<String> letablissementUser = TraitementUtil.getStringToList(sbEtablissement);
		
		List<String> lEdupersonAffiliationWithoutSpace = new ArrayList<String>();
		List<String> letablissementUserWithoutSpace = new ArrayList<String>();
		for(String s : lEdupersonAffiliation) {
			lEdupersonAffiliationWithoutSpace.add(s.trim());
		}
		// on nettoie les établissements en supprimant les espaces  lors de la récupération dans la bdd 
		for(String etablissementUSer : letablissementUser) {
			letablissementUserWithoutSpace.add(etablissementUSer.trim());
		}
		String[] lEtab = properties.get("csv.profil.CU1").toString().split(",");
		
		getProfileUserAff(user, properties, letablissementUserWithoutSpace);
		
		// récupération de la catégorie staff, public
		getRecordTypeUser(letablissementUser,user,properties);
	}
	
	@Override
	public Set<LdapExport> getUserNotExported(List<LdapExport> lUtilisateur,Properties properties) throws SuppanAffectationException {
		
		Set<LdapExport> sUserNotExported = new HashSet<LdapExport>();
		String[] lProfilCsvToExport = properties.get("csv.etablissement").toString().split(",");
		
		for (LdapExport notExported : lUtilisateur) {
			boolean isAffected = false;
			// verification de l'affectation dans le Supannentiteaffectation
			StringBuilder buf = TraitementUtil.deleteFromSupannentiteaffectation(notExported);
			if (buf.length() == 0){
				sUserNotExported.add(notExported);
				//logger.info("ControleAccesService : Pas  de suppanentiteaffectation pour : "+notExported.getMail() +" : "+notExported.getSupannentiteaffectation() );
			}else {
				List<String> lAffectation = TraitementUtil.getStringToList(buf);
				for(String val : lAffectation) {
					if(ArrayUtils.contains(lProfilCsvToExport, val.trim())) {
					//if(properties.containsKey(val.trim())) {
						isAffected = true;
						break;
					}
				}
			}if(!isAffected) {
					//logger.error("**** "+notExported.getCn()+" : "+ notExported.getMail()+" not exported car le suppan entite affectation n'est pas défini ");
					sUserNotExported.add(notExported);
					logger.error("Export PC Securité : ControleAccesService  uid :  "+notExported.getUid()+ "mail =  "+ notExported.getMail() + " n' est affecté à aucun établissement car pas  de suppanentiteaffectation");
					
			}
			
			if(notExported.getO() == "" || notExported.getO()== null 
					|| properties.get("ehess.resident").toString().equals(notExported.getO())) {
				//logger.info("ce user ne sera pas exporté car pas de catég : "+notExported.getMail());
				sUserNotExported.add(notExported);
				logger.error("Export PC Securité : ControleAccesService : "+notExported.getUid()+ " n'est pas exporté car son champs O est vide");
			}
		}
		//logger.info("--- ControleAccesService : Le nombre d'utilisateur non exporté car ne possédant pas de supentiteaffectation\t ou de caegorie est : "+sUserNotExported.size());
		return sUserNotExported;
	}
	
	/**
	 * recupère l eprofile de l'utilisateur
	 * @param user
	 * @param properties
	 * @param lEdupersonAffiliationWithoutSpace
	 */
	public void getProfileUserAff(LdapExport user, Properties properties,List<String> listSupannAffectationtrWithoutSpace) {
		
		if(user.getOu() !=  null || user.getOu()!="") {
			
			String[] lProfilToExport = properties.get("csv.profil.CU1").toString().split(",");
			StringBuilder sbEtablissement = new StringBuilder(user.getSupannentiteaffectation());
			//List<String> letablissementUser = TraitementUtil.getStringToList(sbEtablissement);
			
			/*List<String> listSupannAffectationtrWithoutSpace = new ArrayList<String>();
			// on nettoie les établissements en supprimant les espaces  lors de la récupération au niveau de la bdd 
			for(String etablissementUSer : letablissementUser) {
				listSupannAffectationtrWithoutSpace.add(etablissementUSer.trim());
			}*/
			
			// profil CU1
			for(String supannAffectation : listSupannAffectationtrWithoutSpace) {
				if(ArrayUtils.contains(lProfilToExport, supannAffectation.trim())) {
					user.setUserGroup(RefIdConstants.PROFIL_CU1);
					user.setUserGroupDesc(properties.get("csv.profil.CU1.libelle").toString());
				}
			}
			
			 /**
			  *  On exporte par défaut sur le profil :
			  *  4 utilisateurs universitaires SAUF pour l'INED avec le profil 5-INED
			  *  La règle est SI supannEntiteAffectation=INED    ALORS Profil=5-INED
			  */

		   
			if(null != user.getO()) {
				if("INED".equals(user.getO().trim())){
					user.setPersonalTitle("05-INED-Utilisateurs");
					
				}else {
					user.setPersonalTitle("04-Utilisateurs-Universitaires");
				}
			}
		}
		
	}
	
	private static void getLastExecutedDate(LastExecutionCsv lastrun) {
		Date currentDate = new Date();
		logger.info("-- date du jour : " + dateFormat.format(currentDate));
		Date d = lastrun.getLastdateexecution();
		logger.info("++ date dernière execution  = "+dateFormat.format(d));
		
		// convert date to localdatetime
		LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		logger.info("*** localDateTime : " + dateFormat8.format(localDateTime));
		
		LocalDateTime localDateDernièreExecution = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		
		logger.info("d execution = "+dateFormat8.format(localDateDernièreExecution));
		
		localDateDernièreExecution = localDateDernièreExecution.plusDays(3);
		
		Date DateDernièreExecutionPlusOneDay = Date.from(localDateDernièreExecution.atZone(ZoneId.systemDefault()).toInstant());
		
		logger.info("test 2 jours après : "+dateFormat.format(DateDernièreExecutionPlusOneDay));
		
		// plus one
		localDateTime = localDateTime.plusYears(2).plusMonths(1).plusDays(3);
		//localDateTime = localDateTime.plusHours(1).plusMinutes(2).minusMinutes(1).plusSeconds(1);

		// convert LocalDateTime to date
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

		logger.info("\nOutput : " + dateFormat.format(currentDatePlusOneDay));
		
	}
	
	@Override
	public void loadTemplateForAll(Template template, VelocityContext context) {
	
		try {
			Calendar cal = Calendar.getInstance(); cal.setTime(Date.from(Instant.now()));
			
			String format = String.format( RefIdConstants.FORMAT, cal);
			StringBuilder builder = new StringBuilder();
			builder.append(folderLocation);
			
			builder.append(format);
			builder.append(RefIdConstants.REF_ID_EXPORT_ALL_CSV);
			
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(builder.toString()),"ISO-8859-1"));
			
			template.merge(context, writer);

			writer.flush();
			writer.close();
			//TODO à voir si REF_ID_EXPORT_CSV REF_ID_EXPORT_ALL_CSV dans le cadre de la creation de tpus les utilisateurs 
			File fileCreated = new File(folderLocation+format+RefIdConstants.REF_ID_EXPORT_ALL_CSV);
			if(fileCreated.exists()) {
				logger.info("file is created");
			}else {
				logger.error("could not created");
			}
		//pas besoin de zipper le fichier ni de faire l'export pour le csv
		
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("le fichier n'est pas créé "+e.getMessage());
		}catch (FileAlreadyExistsException  e) {
			logger.error("file already exist");
		}catch (FtpLoginException e) {
			logger.error("connexion serveur erreur");
		}catch (Exception e) {
			logger.info(e);
		}
	}
}
	
