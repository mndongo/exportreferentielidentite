package com.campus.condorcet.service.impl;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.campus.condorcet.dao.IUserDao;
import com.campus.condorcet.model.User;
import com.campus.condorcet.service.IUserService;

@Service("userService")
public class UserServiceImpl implements IUserService{

	@Autowired
	IUserDao userdao;
	


	public void setUserdao(IUserDao userdao) {
		this.userdao = userdao;
	}

	@Transactional
	public User findUSerById() {
		try {
			System.out.println("** start in my service ");
			User user = new User();
			user.setId(1);
			userdao.loadUser();
			//userdao.loadUserByUsername("paula");
			//userdao.findbyId(user);
			return user;			
		}catch (PersistenceException e) {
			System.out.println("erreur lors de la r�cup�ration des donn�es : "+e);
		}
		return null;
	}


}
