package com.campus.condorcet.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilePermission;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeoutException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.campus.condorcet.service.IRefIndentiteUploadService;
import com.campus.condorcet.utils.RefIdConstants;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class RefIdentiteUploadServiceImpl implements IRefIndentiteUploadService {

	private static final Logger logger = Logger.getLogger(RefIdentiteUploadServiceImpl.class);
	
	@Override
	public void export() {
		
	}

	@Override
	public void export(String host, String user, String password,int port, File file) {
			logger.info(" ### Start to export the file :  "+file.getName() + " in : "+ file.getAbsolutePath() + " ####");
			// on export que le fichier du jour
			JSch jsch = new JSch();
			Session session = null;
		
			try {
				session = jsch.getSession(user, host,port);
				logger.info("port is : "+port );
				java.util.Properties config = new java.util.Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
				session.setPassword(password);
				session.setPort(port);
				session.setTimeout(9000);
				session.connect(6660000);
				if (session.isConnected()) {
					logger.info("is connected");
					Channel channel = session.openChannel("sftp");
					ChannelSftp sftpChannel = (ChannelSftp) channel;
					sftpChannel.connect();
					if(sftpChannel.isConnected()) {
						// TODO à externaliser le fichier properties
						sftpChannel.put(new FileInputStream(file), RefIdConstants.SRC_MAIN_RESOURCES_EXPORT_REMOTE_DIR+file.getName());
					}else {
						logger.info("not connected to sftp channel");
					}
					
					sftpChannel.exit();
	           
				}else {
					logger.error("connexion refused");
				}
			}
			catch (JSchException e) {
				e.getCause();
				logger.info("cause = "+e.getCause());
				logger.error("error dowloading the file :"  +e.getMessage());
				
			}catch (SftpException e) {
				logger.error("error dowloading during transfert file"+ e);
	        }catch (Exception e) {
	        	logger.error("error in the protocole : "+e);
			}
			finally {
	        	 session.disconnect();
			}
			logger.info(" ### finish exported file :  "+file.getName() + " ####");
	}
	
}
