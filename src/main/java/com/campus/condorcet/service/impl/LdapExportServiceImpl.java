package com.campus.condorcet.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.FileAlreadyExistsException;
import java.time.DateTimeException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.hibernate.TransactionException;

import com.campus.condorcet.exception.SuppanAffectationException;
import com.campus.condorcet.model.CsvDataExport;
import com.campus.condorcet.model.LdapExport;
import com.campus.condorcet.model.XmlDataExport;
import com.campus.condorcet.utils.RefIdConstants;

import sun.net.ftp.FtpLoginException;


public class LdapExportServiceImpl extends LdapExportService{

	private static final Logger logger = Logger.getLogger(LdapExportServiceImpl.class);
	
	
	/**
	 * get All users
	 * 
	 */
	@Override
	public List<LdapExport> loadReferentielUser(Properties properties,String ops,String date) {
		HashMap<String, String> hUserXmlDejaExporte = new HashMap<String, String>();
		try {
				List<LdapExport> lUtilisateur= null;
				StringBuilder dateDuJour = TraitementUtil.getdateDuJourFormatAlma();
				//lUtilisateur= ldapdao.loadAll();
				lUtilisateur= ldapdao.loadAllByDateModification(date, ops, 0);
				if(lUtilisateur.size()==0) {
					logger.error("Il n'y pas d'utilisateur a exporter vers alma ");
					
				}else {
					Set<LdapExport> sUserdoNotExport = new HashSet<LdapExport>();
					Set<LdapExport> sUserNotHavingEduPeronnAfiliation = new HashSet<LdapExport>();
					
					sUserdoNotExport= getUserNotExported(lUtilisateur, properties);
					
					logger.info("+++++ le nombre d'utilisateur initial = "+lUtilisateur.size());
					
					// on purge les utilisateur qui n'ont pas de supannentite affectation de la liste des utilisateurs initiale.
					for (LdapExport ldapExport : sUserdoNotExport) {
						if(lUtilisateur.contains(ldapExport)) {
							logger.error("Export Alma : utilisateur sera supprimer de la liste initiale car pas suppanentiteaffectationn uid =: "+ldapExport.getUid()+ " mail =  "+ldapExport.getMail());
							lUtilisateur.remove(ldapExport);
						}
					}
					logger.error("*** Export Alma :  Nombre d'utilistateur total n'ayant pas de suppanentiteaffectationn et ne sera pas exporte : "+sUserdoNotExport.size());
					
					
					initUser(properties, lUtilisateur, dateDuJour, sUserNotHavingEduPeronnAfiliation);
				
					// suppression des users qui n'ont pas d'affiliation(lors de l'initailisation du usergroup)		
					for (LdapExport ldapExportNotHavingEduPersonAffiliation : sUserNotHavingEduPeronnAfiliation) {
						if(lUtilisateur.contains(ldapExportNotHavingEduPersonAffiliation)) {
							logger.error("EXport Alma : l'utilisateur  : "+ldapExportNotHavingEduPersonAffiliation.getUid()+ " n'est pas exporté car un des champs est manquant");
							lUtilisateur.remove(ldapExportNotHavingEduPersonAffiliation);
						}
					}
					if(lUtilisateur.size()==0) {
						logger.debug("LdapExportService : Après traitement, il n'y a pas d'export xml ");
					}
					else {
						// sauvegade des utilisateurs exportés
						logger.info("nombre d'utilisateur a sauvegarder = "+lUtilisateur.size());
						saveXmlUserExported(lUtilisateur);
						createTemplateType(lUtilisateur);
						logger.info("nbre d'utilisateur réaliser pour l'export xml = "+lUtilisateur.size());
						
					}
					
				}
				
		}catch (DateTimeException e) {
			logger.error("erreur lors de la creation de la date");
		}catch(TransactionException e) {
			logger.error("problème de connexion à la base");
		}
		catch (Exception e) {
			logger.info(" error = " +e);
		}
		
		return null;
	}
	/**
	 * sauvegarde des utilisateurs déjà exportés
	 * @param lUtilisateur
	 */
	private void saveXmlUserExported(List<LdapExport> lUtilisateur) {
		//logger.info("  start saveXmlUserExported  ");
		String dateDuJour = TraitementUtil.getdateDuJourFormat();
		HashSet<LdapExport> setUtilisateur = new HashSet<LdapExport>();
		Iterator<LdapExport> it = lUtilisateur.iterator();
		while(it.hasNext()) {
			setUtilisateur.add(it.next());
		}
		
		for(LdapExport exp : setUtilisateur) {
			//logger.info("save user : "+exp.getUid());
			XmlDataExport xmlDataExport = new XmlDataExport();
			xmlDataExport.setUid(exp.getUid());
			xmlDataExport.setDateeexport(dateDuJour);
			xmlDataExport.setMail(exp.getMail());
			ldapdao.save(xmlDataExport);
			//logger.info("end save user : "+exp.getUid());
		}
	
		logger.info(" end saveXmlUserExported  ");
	}

	/**
	 * cr�ation et chargement du fichier xml à partir des donn�es utilisateurs
	 * @param template
	 * @param context
	 * @throws IOException
	 */
	
	/**
	 * TODO à supprimer et le laisser dans la classe maère
	 * @param fileCreated
	 * @return
	 * @throws IOException
	 */
	public  File zipFile(File fileCreated) throws IOException {
		  
		try {
			
			// input file 
	        FileOutputStream fos = new FileOutputStream(folderLocation+RefIdConstants.ZIP);
	        // out put file 
	        ZipOutputStream zos = new ZipOutputStream(fos);
	        ZipEntry ze= new ZipEntry(fileCreated.getName());
	        // name the file inside the zip  file 
	        zos.putNextEntry(ze); 
    		FileInputStream in = new FileInputStream(fileCreated);
	        // buffer size
	        byte[] b = new byte[1024];
	        int count;
	        while ((count = in.read(b)) > 0) {
	            zos.write(b, 0, count);
	        }
	        in.close();
	        zos.closeEntry();
	        zos.close();
	        
	        File directory = new File(folderLocation);
	        File[] fList = directory.listFiles();
	        for(File f : fList) {
	        	if(RefIdConstants.ZIP.equals(f.getName())) {
	        		return f;
	        	}
	        }
	        
	        
		}catch (Exception e) {
			 logger.error("Exception when trying to zip the logs: " +e);
		
		}
			
		return null;
		
}

	@Override
	public void createTemplateType(List<LdapExport> lUtilisateur) {
		logger.info("start createTemplateXml  dans la classe mère");
		
		/**
		 * Initialize engine and get template
		 */
		
		try{
			Properties p = new Properties();	
			p.setProperty("resource.loader", "class");
			p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			p.setProperty("input.encoding", CharEncoding.UTF_8);
			p.setProperty("output.encoding",CharEncoding.UTF_8);
			p.setProperty("response.encoding",CharEncoding.UTF_8);
			p.setProperty("default.contentType", "text/html; charset=UTF-8");

			Velocity.init( p ); 
			VelocityContext context = new VelocityContext();
			
			Template template = Velocity.getTemplate("templates/RefId_export.vm");
			template.setEncoding(CharEncoding.UTF_8);
			// récupération de la template
			if(template != null) {
				context.put("userList", lUtilisateur);
				context.put("total", lUtilisateur.size());
				
				// chargement de la template
				loadTemplateT(template, context);
				// ecriture de la template dans la console
				StringWriter swOut = new StringWriter();
				template.merge(context, swOut);
			}
			
		}catch( ResourceNotFoundException rnfe ){
			logger.error("la ressource demandée n'existe pas " +rnfe);
		}catch( ParseErrorException pee ) {
			logger.error("une erreur est survenue lors du parsing de la template " +pee);
		}catch( MethodInvocationException mie ){
			logger.info("erreur est apparue lors d'un appel à une directive "+mie);

		}
		logger.info("Fin createTemplateXml  dans la classe mère");
	}

	
	public Set<LdapExport> getUserNotExported(List<LdapExport> lUtilisateur,Properties properties) throws SuppanAffectationException {
		logger.info("*** LdapExportServiceImpl-getUserNotExported : Start get user not exported dans ldapEXport ****");
		Set<LdapExport> sUserNotExported = new HashSet<LdapExport>();
		String[] lProfilToExport = properties.get("alma.etablissement").toString().split(",");
		for (LdapExport notExported : lUtilisateur) {
			boolean isAffected = false;
			// verification de l'affectation dans le Supannentiteaffectation
			StringBuilder buf = TraitementUtil.deleteFromSupannentiteaffectation(notExported);
			if (buf.length() == 0){
				sUserNotExported.add(notExported);
				logger.info("EXport Alma : LdapExportServiceImpl : l'utilisateur "+ notExported.getUid() +" mail = : "+notExported.getMail()+" n'est pas exporté  car pas de Supannentiteaffectation" );
				
			}else {
				List<String> lAffectation = TraitementUtil.getStringToList(buf);
				for(String valueOfSupannEntiteAfectation : lAffectation) {
					// si etablissement est dans la liste des suppanentiteaffectations
					if(ArrayUtils.contains(lProfilToExport, valueOfSupannEntiteAfectation.trim())) {
						isAffected = true;
						break;
					}
					
				}
			}if(!isAffected) {
				sUserNotExported.add(notExported);
				logger.error("EXport Alma : LdapExportServiceImpl : l'utilisateur uid = "+notExported.getUid()+" mail = " +notExported.getMail()+ " n' est affecté à aucun établissement car pas  de suppanentiteaffectation : ");
			}
		}
		
		logger.info("LdapExportServiceImpl : Le nombre d'utilisateur qui ne sera pas exporté est : "+sUserNotExported.size());
		return sUserNotExported;
	}

	@Override
	public void loadTemplateT(Template template, VelocityContext context) {
		try {
			Calendar cal = Calendar.getInstance(); cal.setTime(Date.from(Instant.now()));
			
			String format = String.format( RefIdConstants.FORMAT, cal);
			StringBuilder builder = new StringBuilder();
			builder.append(folderLocation);
			builder.append(format);
			builder.append(RefIdConstants.REF_ID_EXPORT_XML);
			FileWriter writer = new FileWriter(new File(builder.toString()));
			
			template.merge(context, writer);
			
			
			writer.flush();
			writer.close();
			File fileCreated = new File(folderLocation+format+RefIdConstants.REF_ID_EXPORT_XML);
			if(fileCreated.exists()) {
				logger.info("file is created");
			}else {
				logger.error("could not created");
			}
			
			File fileZip = zipFile(fileCreated);
			// TODO dé-commener pour la mise en prod
			refIdentiteUploadService.export(host,user,password,Integer.valueOf(port),fileZip);
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("le fichier n'est pas créé "+e.getMessage());
		}catch (FileAlreadyExistsException  e) {
			logger.error("file already exist");
		}catch (FtpLoginException e) {
			logger.error("connexion serveur erreur");
		}catch (Exception e) {
			logger.info(e);
		}
		
	}

	/**
	 * recupère le profile de l'utilisateur
	 * @param user
	 * @param properties
	 * @param lEdupersonAffiliationWithoutSpace
	 */
	public void getProfileUserAff(LdapExport user, Properties properties,List<String> lEdupersonAffiliationWithoutSpace) {

		StringBuilder sbEtablissement = new StringBuilder(user.getSupannentiteaffectation());
		// liste des etablissement dans Supannentiteaffectation
		List<String> letablissementUser = TraitementUtil.getStringToList(sbEtablissement);
		// on nettoie la liste des des etablissements avec la suppression des espaces
		List<String> letablissementUserWithoutSpace = new ArrayList<String>();
		String[] almaEtablissement = properties.get("alma.etablissement").toString().split(",");
		
		for(String etablissementUSer : letablissementUser) {
			letablissementUserWithoutSpace.add(etablissementUSer.trim());
		}
		// par defaut le profil est CU1 
		// Résidents : profile CU1
		for(String nomEtablissement : letablissementUserWithoutSpace) {
			if(ArrayUtils.contains(almaEtablissement, nomEtablissement)){
				user.setUserGroup(properties.get("profil.CU1").toString());
				user.setUserGroupDesc(properties.get("alma.profil.CU1.libelle").toString());
				break;
				
			}
		}
		// Résidents : profils CU2
		String[] lProfilCU21 = properties.get("alma.profil.CU2").toString().split(",");
		
		for(String nomEtablissement : letablissementUserWithoutSpace) {
			if(ArrayUtils.contains(lProfilCU21, nomEtablissement.substring(0, 2))) {
				user.setUserGroup(properties.get("profil.CU2").toString());
				user.setUserGroupDesc(properties.get("alma.profil.CU2.libelle").toString());	
				break;
			}
		}
		
		// GED LECTEURS 
		if(user.getOu() != null && user.getOu().equals(properties.get("alma.ged.lecteurs").toString())) {
			
			String[] almaProfilGedLecteurCu1Cu2 = properties.get("alma.etablissement.ged.lecteur.cu1-cu2").toString().split(",");
			String[] almaProfilGedLecteurCu3Cu4 = properties.get("alma.etablissement.ged.lecteur.cu3-cu4").toString().split(",");
			String[] almaProfilGedLecteurCu5 = properties.get("alma.etablissement.ged.lecteur.cu5").toString().split(",");
			for(String nomEtablissement : letablissementUserWithoutSpace) {
				// GED LECTEURS CU2
				if(ArrayUtils.contains(almaProfilGedLecteurCu1Cu2, nomEtablissement) && lEdupersonAffiliationWithoutSpace.contains(RefIdConstants.STUDENT)) {
					user.setUserGroup(properties.get("profil.CU2").toString());
					user.setUserGroupDesc(properties.get("alma.profil.CU2.libelle").toString());
					
				}
				// GED LECTEURS CU1
				else if(ArrayUtils.contains(almaProfilGedLecteurCu1Cu2, nomEtablissement) && !lEdupersonAffiliationWithoutSpace.contains(RefIdConstants.STUDENT)) {
					user.setUserGroup(properties.get("profil.CU1").toString());
					user.setUserGroupDesc(properties.get("alma.profil.CU1.libelle").toString());
					break;
				}
				// GED LECTEURS CU3
				else if(ArrayUtils.contains(almaProfilGedLecteurCu3Cu4, nomEtablissement) && !lEdupersonAffiliationWithoutSpace.contains(RefIdConstants.STUDENT)) {
					logger.info("profil cu3 pour : "+user.getUid());
					user.setUserGroup(properties.get("profil.CU3").toString());
					user.setUserGroupDesc(properties.get("alma.profil.CU3.libelle").toString());
				}
				// GED LECTEURS CU4
				else if(ArrayUtils.contains(almaProfilGedLecteurCu3Cu4, nomEtablissement) && lEdupersonAffiliationWithoutSpace.contains(RefIdConstants.STUDENT)) {
					logger.info("profil cu4 pour : "+user.getUid());
					user.setUserGroup(properties.get("profil.CU4").toString());
					user.setUserGroupDesc(properties.get("alma.profil.CU4.libelle").toString());
				}
				// GED LECTEURS CU5
				else if(ArrayUtils.contains(almaProfilGedLecteurCu5, nomEtablissement)) {
					logger.info("profil cu4 pour : "+user.getUid());
					user.setUserGroup(properties.get("profil.CU5").toString());
					user.setUserGroupDesc(properties.get("alma.profil.CU5.libelle").toString());
				}
				
			}
		}
		
	}
	
	/**
	 * 
	 * @param user
	 * @param properties
	 */
	public void getAffiliation(LdapExport user,Properties properties) {
		
		TraitementUtil.deleteCrochetForUser(user);
		// récupération et traitement de edupersonaffiliation sbAffiliation
		StringBuilder sbAffiliation = new StringBuilder(user.getEduPersonAffiliation().trim());
		StringBuilder sbEtablissement = new StringBuilder(user.getSupannentiteaffectation());
		PropertiesConfiguration p = null;
		List<String> lEdupersonAffiliation = TraitementUtil.getStringToList(sbAffiliation);
		List<String> letablissementUser = TraitementUtil.getStringToList(sbEtablissement);
		
		List<String> lEdupersonAffiliationWithoutSpace = new ArrayList<String>();
		List<String> letablissementUserWithoutSpace = new ArrayList<String>();
		for(String s : lEdupersonAffiliation) {
			// liste edupersonaffiliation
			lEdupersonAffiliationWithoutSpace.add(s.trim());
		}
		// on nettoie les établissements en supprimant les espaces  lors de la récupération au niveau de la bdd 
		for(String etablissementUSer : letablissementUser) {
			// liste suppanentiteaffectation
			letablissementUserWithoutSpace.add(etablissementUSer.trim());
		}
		
		getProfileUserAff(user, properties, lEdupersonAffiliationWithoutSpace);
			
		// récupération de la catégorie staff, public
		getRecordTypeUser(letablissementUser,user,properties);
		
	}
}
