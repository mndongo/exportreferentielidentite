package com.campus.condorcet.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.campus.condorcet.dao.ILdapExportInterfaceDao;
import com.campus.condorcet.exception.SuppanAffectationException;
import com.campus.condorcet.model.LdapExport;
import com.campus.condorcet.service.ILdapExportService;
import com.campus.condorcet.service.IRefIndentiteUploadService;
import com.campus.condorcet.utils.RefIdConstants;
@Service("ldapService")
public abstract class LdapExportService implements ILdapExportService{

	public abstract void createTemplateType(List<LdapExport> lUtilisateur);
	public abstract void loadTemplateT(Template template, VelocityContext context); 
	 
	private static final Logger logger = Logger.getLogger(LdapExportService.class);
	@Autowired
	ILdapExportInterfaceDao ldapdao;
	
	@Autowired 
	IRefIndentiteUploadService refIdentiteUploadService;
	
	@Value("${ftp.ftpuserId}")
	protected String user;
	
	@Value("${ftp.ftpPassword}")
	protected String password;
	
	@Value("${ftp.folderLocation}")
	protected String folderLocation;
	
	@Value("${ftp.ftpPort}")
	protected String port;
	
	
	@Value("${jdbc.url}")
	protected String url;
	
	@Value("${jdbc.username}")
	protected String username;
	
	@Value("${jdbc.password}")
	protected String pwd;
	/*
	@Value("${ftp.folderOut}")
	private String folderOut;*/
	
	
	//protected int i=0;

	public void setLdapdao(ILdapExportInterfaceDao ldapdao) {
		this.ldapdao = ldapdao;
	}
	@Value("${ftp.ftpHost}")
	protected String host;


	@Override
	public List<LdapExport> loadReferentielUser(Properties properties, String dateModif, String op) {
		return null;
	}
	
	/**
	 * 
	 * @param properties
	 * @param lUtilisateur
	 * @param dateDuJour
	 * @param sUserNotHavingEmployerNumberOrGroup
	 */
	public void initUser(Properties properties, List<LdapExport> lUtilisateur, StringBuilder dateDuJour,
			Set<LdapExport> sUserNotHavingEmployerNumberOrGroup) {
		lUtilisateur.stream().forEach((user)->{
			// set la date de fin de contrat
			//String dateForContract = TraitementUtil.getExpiryDate(user);
			//user.setFdContractEndDate(dateForContract);
			// set la date du jour pour chaque utilisateur dans la template
			user.setStatusDate(dateDuJour.toString());
			
			/** transformation des dates issue du ldap en format alma 
			 * YYYYMMJJ-Z dans le ldap mais il faut la modifier 
			 * et la transformer en YYYY-MM-JJ 
			 */
			if(user.getCompteEndDate() != null && user.getCompteEndDate().length() <10) {
				String dateEndCompte = TraitementUtil.getFormatXmlDate(user.getCompteEndDate());
				user.setCompteEndDate(dateEndCompte);
			}
			if(user.getCreateTimestamp() != null && user.getCreateTimestamp().length() <10) {
				String dateCreateTimestamp = TraitementUtil.getFormatXmlDate(user.getCreateTimestamp());
				user.setCreateTimestamp(dateCreateTimestamp);
			}
			
			if(user.getFdContractEndDate() != null && user.getFdContractEndDate().length() <10) {
				String dateFdContractEndDate = TraitementUtil.getFormatXmlDate(user.getFdContractEndDate());
				user.setFdContractEndDate(dateFdContractEndDate);
			}
			if(user.getFdContractStartDate() != null && user.getFdContractStartDate().length() <10) {
				String dateFdContractStartDate = TraitementUtil.getFormatXmlDate(user.getFdContractStartDate());
				user.setFdContractStartDate(dateFdContractStartDate);
			}
			
			
			/** mail perso, phones tests effectués depuis la template */
			// determine l'affectation et initialise le usergroup
			if(null != user.getEduPersonAffiliation() || !user.getEduPersonAffiliation().isEmpty()) {
				getAffiliation(user,properties);
			}
			if (null == user.getUserGroup() || user.getUserGroup().isEmpty() || user.getUserGroup() =="") {
				sUserNotHavingEmployerNumberOrGroup.add(user);
			}
			// utilisateur qui n'ont pas d'employeeNumber
			/**
			 * 25/01/2021
			 * désactivation de cette règle en attendant de voir. ?
			 * pour le moment je sais pas pourquoi certains compte qu'on créé n'ont pas de numero.
			 * Si je trouve comment faire pour que tous les comptes aient un numéro alors on pourra reactiver la regle.

			 */
			if(null == user.getEmployeeNumber() ||user.getEmployeeNumber().isEmpty() ) {
				sUserNotHavingEmployerNumberOrGroup.add(user);
			}
			// determine user identifier
			if(null != user.getFdBadge()) {
				user.setFdBadge(user.getFdBadge());
			}
			// determine user statistic category modification du 10/02/2021 on sette statisticCategory la valeur de supannentiteaffectationprincipale
			if(null != user.getSupannentiteaffectationprincipale()) {
				user.setStatisticCategory(user.getSupannentiteaffectationprincipale());
				user.setCategoryType(RefIdConstants.CATEG_ETABLISSEMENT);
			}else  if(null != user.getOu()) {
				user.setStatisticCategory(RefIdConstants.STAISTIC_CATEG_OU);
				user.setCategoryType(RefIdConstants.CATEG_LABORATOIRE);
			}else {
				user.setStatisticCategory("");
				user.setCategoryType("");
			}
			
		
		});
		logger.error("***  nbre d'utilisateur total n'ayant pas de user group ou d'employer number et ne sera pas exportés :  "+sUserNotHavingEmployerNumberOrGroup.size());
	}
	
	/**
	 * 
	 * @param user
	 * @param properties
	 */
	public void getAffiliation(LdapExport user,Properties properties) {
		
	}
	
	/**
	 * recupère le profile de l'utilisateur
	 * @param user
	 * @param properties
	 * @param lEdupersonAffiliationWithoutSpace
	 */
	public void getProfileUserAff(LdapExport user, Properties properties,List<String> lEdupersonAffiliationWithoutSpace) {
		
	}
	/**
	 * return les utilisateurs qui ne seront pas exportés
	 * @param lUtilisateur
	 * @param properties
	 * @return
	 * @throws SuppanAffectationException
	 */
	public Set<LdapExport> getUserNotExported(List<LdapExport> lUtilisateur,Properties properties) throws SuppanAffectationException {
		return null;
	}

	/**
	 * 
	 * @param lAffectation
	 * @param user
	 * @param properties
	 */
	public void getRecordTypeUser(List<String> lAffectation, LdapExport user,Properties properties) {
		String[] lProfilRecordType = properties.get("profil.record").toString().split(",");
		for(String valAffectation : lAffectation) {
			if(ArrayUtils.contains(lProfilRecordType, valAffectation.trim())) {
				user.setRecordType(properties.getProperty("profil.record.libelle.staff"));
				break;
			}else {
				user.setRecordType(properties.getProperty("profil.record.libelle.public"));
			}
		}
		
	}
	
	
	// zip le fichier
	
	public  File zipFile(File fileCreated) throws IOException {
		 ZipOutputStream out = null;
		try {
			
			// input file 
	        FileInputStream in = new FileInputStream(fileCreated);
	        // out put file 
	        out = new ZipOutputStream(new FileOutputStream(folderLocation+RefIdConstants.ZIP));
	        // name the file inside the zip  file 
	        out.putNextEntry(new ZipEntry(fileCreated.getName())); 
	        // buffer size
	        byte[] b = new byte[1024];
	        int count;
	        while ((count = in.read(b)) > 0) {
	            out.write(b, 0, count);
	        }
	        
	        File directory = new File(folderLocation);
	        File[] fList = directory.listFiles();
	        for(File f : fList) {
	        	if(RefIdConstants.ZIP.equals(f.getName())) {
	        		return f;
	        	}
	        }
	        
	        out.close();
	        in.close();
		}catch (Exception e) {
			 logger.error("Exception when trying to zip the logs: " + e.getMessage());
			 out.closeEntry();
		}
			
		return null;
		
	}
	
	/**
	 * construit une template pour tous les utilisateurs déjà exportés
	 * @param template
	 * @param context
	 */
	public void loadTemplateForAll(Template template, VelocityContext context) {
		
		
	}
	
}
