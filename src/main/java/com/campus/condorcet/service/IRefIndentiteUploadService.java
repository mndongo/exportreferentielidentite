package com.campus.condorcet.service;

import java.io.File;

public interface IRefIndentiteUploadService {
	public void export();

	public void export(String host, String user, String password,int port,File file);
}
