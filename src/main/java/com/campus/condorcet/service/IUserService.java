package com.campus.condorcet.service;

import org.springframework.transaction.annotation.Transactional;

import com.campus.condorcet.model.User;

public interface IUserService {

	@Transactional
	public User findUSerById();
}
